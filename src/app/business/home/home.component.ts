import { DataService } from './../../services/data.service';
import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent
  implements
    OnInit,
    OnChanges,
    DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy
{
  date: Date = new Date();
  data: any[] = [];

  sub!: Subscription;

  constructor(private dataService: DataService) {
    console.log('Hello from the constructor of homeComponent');
  }

  ngDoCheck(): void {
    console.log('ngDoCheck');
  }
  ngAfterContentInit() {
    console.log('ngAfterContentInit');
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges' + changes);
  }

  ngAfterContentChecked() {
    console.log('ngAfterContentChecked');
  }

  ngOnInit(): void {
    console.log('ngOnInIt');

    this.dataService.getData().subscribe((responseData) => {
      this.data = responseData;
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  ngAfterViewInit() {
    console.log('ngAfterViewInit');
  }

  ngAfterViewChecked() {
    console.log('ngAfterViewChecked');
  }

  selectedBookId(event: any) {
    console.log(event);
    alert(event);
  }
}
