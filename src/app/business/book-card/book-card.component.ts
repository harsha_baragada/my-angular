import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.css'],
})
export class BookCardComponent implements OnInit {
  @Input() data: any;
  @Output() selectedId = new EventEmitter<any>();
  
  constructor() {}

  ngOnInit(): void {}

  selectBook(book: any) {
    console.log(book.id);
    this.selectedId.emit(book.id);
  }
}
