import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessRoutingModule } from './business-routing.module';
import { HomeComponent } from './home/home.component';
import { BookCardComponent } from './book-card/book-card.component';

@NgModule({
  declarations: [HomeComponent, BookCardComponent],
  imports: [CommonModule, BusinessRoutingModule, SharedModule],
})
export class BusinessModule {}
