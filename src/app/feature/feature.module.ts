import { SharedModule } from './../shared/shared.module';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SecondComponent } from './second/second.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureRoutingModule } from './feature-routing.module';


@NgModule({
  declarations: [HomeComponent, SecondComponent, SignUpComponent],
  imports: [
    CommonModule,
    FeatureRoutingModule,
    SharedModule
  ]
})
export class FeatureModule { }
