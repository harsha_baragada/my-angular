import { DataService } from '../../services/data.service';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [],
})
export class HomeComponent implements OnInit {
  //private String title;
  thisIsAVariable: string =
    'The online encyclopedia project, Wikipedia, is the most popular wiki-based website, and is one of the most widely viewed sites in the world, having been ranked in the top twenty since 2007.[3] Wikipedia is not a single wiki but rather a collection of hundreds of wikis, with each one pertaining to a specific language.';
  userName: string = 'please enter your user name';
  data: any[] = [];
  constructor(private readonly dataService: DataService) {}

  ngOnInit(): void {
    this.dataService.getData().subscribe((respose) => {
      console.log(respose);
      this.data = respose;
    });
  }
}
