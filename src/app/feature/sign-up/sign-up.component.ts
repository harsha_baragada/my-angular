import { DataService } from './../../services/data.service';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent implements OnInit {
  signUpForm!: FormGroup;
  matcher = new MyErrorStateMatcher();

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.signUpForm = this.createSignUpForm();
    console.log(this.signUpForm);
  }

  createSignUpForm(): FormGroup {
    return new FormGroup({
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      passwordFormControl: new FormControl('', [Validators.required]),
    });
  }

  signUp() {
    console.log(this.signUpForm);

    let user = {
      emailId: this.signUpForm.value.emailFormControl,
      pwd: this.signUpForm.value.passwordFormControl,
    };
    console.log(user);
    this.dataService.signUp(user).subscribe((res) => {
      console.log(res);
    });
  }
}
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}
