import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css'],
})
export class SecondComponent implements OnInit {
  togglePara!: boolean;
  constructor() {}

  ngOnInit(): void {}

  toggleParagraph() {
    this.togglePara = !this.togglePara;
  }
}
